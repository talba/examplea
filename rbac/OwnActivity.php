<?php
namespace app\rbac;

use yii\rbac\Rule;
use Yii; 
use app\models\User;

class OwnUserRule extends Rule
{
	public $name = 'OwnCategoryRule';

	public function execute($user, $item, $params)
	{
        $userModel = User::findIdentity($user);
		if (!Yii::$app->user->isGuest) {
			return isset($params['activity']) ? $params['activity']->categoryID == $userModel->categoryID : false;
		}
		return false;
	}
}