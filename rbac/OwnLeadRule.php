<?php
namespace app\rbac;

use yii\rbac\Rule;
use Yii; 

class OwnLeadRule extends Rule
{
	public $name = 'ownLeadRule';

	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			return isset($params['lead']) ? $params['lead']->owner == $user : false;
			//$params['lead']=Lead object, owner of lead object is integer (id number of user tha is lead owner) 
		}
		return false;
	}
}