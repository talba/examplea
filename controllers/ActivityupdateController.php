<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;


class ActivityupdateController extends Controller
{
	public function actionAddrule()
	{
		$auth = Yii::$app->authManager;				
		$updateOwnActivity = $auth->getPermission('updateOwnActivity');
		
		$rule = new \app\rbac\OwnActivity;
		$auth->add($rule);
				
		$updateOwnActivity->rule_name = $rule->name;
        
	}
}