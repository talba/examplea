<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;


class RbacController extends Controller
{
	public function actionRole()
	{
		$auth = Yii::$app->authManager;				
		
		$categoryManager = $auth->createRole('categoryManager');
		$auth->add($categoryManager);
		
		$authorized = $auth->createRole('authorized');
		$auth->add($authorized);

	}

	/*public function actionGpermission()
	{
		$auth = Yii::$app->authManager;
		
		$indexUser = $auth->createPermission('indexUser');
		$indexUser->description = 'All users can view users';
		$auth->add($indexUser);
	
	}*/


	public function actionApermissions()
	{
		$auth = Yii::$app->authManager;
		
		$createActivity = $auth->createPermission('createActivity');
		$createActivity->description = 'Authorizrd user can create new activity';
		$auth->add($createActivity);
		
				
	}
	

	public function actionCmpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$updateOwnActivity = $auth->createPermission('updateOwnActivity');
		$updateOwnActivity->description = 'Category manager can update activity in own category';
		$auth->add($updateOwnActivity);
	}

	public function actionAdminpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$updateActivity = $auth->createPermission('updateActivity');
		$updateActivity->description = 'Admin can update all activities';
		$auth->add($updateActivity);
	
	}


	public function actionChilds()
	{
		$auth = Yii::$app->authManager;				
		
		$authorized = $auth->getRole('authorized');
		$admin = $auth->getRole('admin');

		$createActivity = $auth->getPermission('createActivity');
		$auth->addChild($authorized, $createActivity);

		$categoryManager = $auth->getRole('categoryManager');
		$auth->addChild($categoryManager, $authorized);

		$updateOwnActivity = $auth->getPermission('updateOwnActivity');
		$auth->addChild($categoryManager, $updateOwnActivity);

		$updateActivity = $auth->getPermission('updateActivity');
		$auth->addChild($admin, $updateActivity);

		$auth->addChild($admin, $categoryManager);

		
	}
}