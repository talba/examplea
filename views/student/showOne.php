<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'Show One Student';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        The name of the student is <?= $name ?>
    </p>
</div>