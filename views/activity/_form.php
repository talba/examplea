<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Status;
use app\models\Category;

/* @var $this yii\web\View */
/* @var $model app\models\Activity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activity-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'categoryID')->dropDownList(Category::getCategories()) ?> 
    
    <?= $model->isNewRecord ? "" : $form->field($model, 'statusID')->dropDownList(Status::getStatuses()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
