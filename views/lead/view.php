<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Lead */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Leads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lead-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if (\Yii::$app->user->can('updateLead') ||  
		    \Yii::$app->user->can('updateOwnLead', ['lead' =>$model]) ) { ?>
		<?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
			<?php } ?>
			
		<?php if (\Yii::$app->user->can('updateLead'))  { ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'email:email',
            'phone',
            'notes:ntext',
            [ // the status name
				'label' => $model->attributeLabels()['status'],
				'value' => $model->statusItem->name,			
			],
			[ // the owner name of the lead
				'label' => $model->attributeLabels()['owner'],
				'format' => 'html',
				'value' => Html::a($model->userOwner->fullname, 
					['user/view', 'id' => $model->userOwner->id]),			
			],
			[ // created at
				'label' => $model->attributeLabels()['created_at'],
				'value' => date("Y-m-d H:i:s", $model->created_at), 		
			],
			
            [ // updated at
				'label' => $model->attributeLabels()['updated_at'],
				'value' => date("Y-m-d H:i:s", $model->updated_at), 		
			],
			
			[ // created by full name
				'label' => $model->attributeLabels()['created_by'],
				'format' => 'html',
				'value' => Html::a($model->createdBy->fullname, 
					['user/view', 'id' => $model->createdBy->id]),			
			],
			[ // updated by full name
				'label' => $model->attributeLabels()['updated_by'],
				'format' => 'html',
				'value' => Html::a($model->updatedBy->fullname, 
					['user/view', 'id' => $model->updatedBy->id]),			
			],
        ],
    ]) ?>

</div>
