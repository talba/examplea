<?php

namespace app\models;

use Yii;
	use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "activity".
 *
 * @property integer $id
 * @property string $title
 * @property integer $categoryID
 * @property integer $statusID
 */
class Activity extends \yii\db\ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'categoryID', ], 'required'],
            [['categoryID', 'statusID'], 'integer'],
            [['title'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'categoryID' => 'Category ID',
            'statusID' => 'Status ID',
        ];
    }

    public function getStatusID()
    {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }

    public function getCategories()
    {
        return $this->hasOne(Category::className(), ['id' => 'category']);
    }

    public static function getActivities()
	{
		$allActivities = Activity::find()->all();
		$allActivitiesArray = ArrayHelper::
					map($allActivities, 'id', 'title');
		return $allActivitiesArray;						
	}
    

    public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

        if ($this->isNewRecord){
            $this->statusID=2;
        }
        
        return $return;
    }

}
